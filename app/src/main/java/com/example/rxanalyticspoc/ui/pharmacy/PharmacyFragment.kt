package com.example.rxanalyticspoc.ui.pharmacy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.rxanalyticspoc.R
import com.google.android.material.button.MaterialButton
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

class PharmacyFragment : Fragment() {

    private lateinit var pharmacyViewModel: PharmacyViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        pharmacyViewModel = ViewModelProvider(this).get(PharmacyViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_pharmacy, container, false)
        root.findViewById<MaterialButton>(R.id.pharmacyButton).setOnClickListener {
            Firebase.analytics.logEvent("MY_PHARMACY_EVENT") {
                param("isRx", "true")
            }
        }
        return root
    }

    override fun onResume() {
        super.onResume()
        Firebase.analytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, "PharmacyScreen")
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "Main")
            param("isRx", "true")
        }
    }
}