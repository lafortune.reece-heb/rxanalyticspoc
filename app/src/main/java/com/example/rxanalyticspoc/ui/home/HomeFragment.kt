package com.example.rxanalyticspoc.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.rxanalyticspoc.R
import com.google.android.material.button.MaterialButton
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        root.findViewById<MaterialButton>(R.id.eCommButton).setOnClickListener {
            Firebase.analytics.logEvent("MY_ECOMM_EVENT") {}
        }
        root.findViewById<MaterialButton>(R.id.pharmacyButton).setOnClickListener {
            Firebase.analytics.logEvent("MY_PHARMACY_EVENT") {
                param("isRx", "true")
            }
        }
        return root
    }

    override fun onResume() {
        super.onResume()
        Firebase.analytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, "eCommScreen")
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "Main")
        }
    }
}
