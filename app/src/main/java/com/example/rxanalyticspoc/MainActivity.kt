package com.example.rxanalyticspoc

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import java.util.UUID

class MainActivity : AppCompatActivity() {
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController =
            (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_pharmacy
            )
        )
        navView.setupWithNavController(navController)

        firebaseAnalytics = Firebase.analytics
        val sp = getSharedPreferences("analytics-ids", Context.MODE_PRIVATE)
        firebaseAnalytics.setUserId(sp.getOrSet("userId") { UUID.randomUUID().toString() })
        Log.d("FirebaseAnalytics-Main", "userId: ${sp.getString("userId", "(null)")}")
    }

    private fun SharedPreferences.getOrSet(key: String, valueFun: () -> String): String {
        return getString(key, null) ?: run {
            val value = valueFun()
            edit { putString(key, value) }
            value
        }
    }

    override fun onKeyLongPress(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://tagmanager.google.com/mcpr/com.example.rxanalyticspoc?id=GTM-TTWS78D&gtm_auth=mIHfdRLuyzcJV3BpF8GC7g&gtm_preview=1")
                )
            )
        }
        return super.onKeyLongPress(keyCode, event)
    }
}
